<?php

return array(

    'workers' => array(

        'Trusteeship' => array(
            'protocol'              => 'tcp',
            'port'                  => 2202,
            'child_count'           => 201,
            'ip'                    => "0.0.0.0",
            'recv_timeout'          => 10000,
            'process_timeout'       => 5000,
            'send_timeout'          => 1000,
            'persistent_connection' => false,
           // 'max_requests'          => 1000,
            'max_work_time'          => 120,
            'worker_class'          => 'JmTextWorker',
            'bootstrap'             => '/home/www/TrusteeshipData/init.php',
            'slowlog_time'          => 3000,
        ),

        // 统计接口调用结果 只开一个进程 已经配置好，不用设置
        'StatisticWorker' => array(
            'protocol'              => 'udp',
            'port'                  => 2207,
            'child_count'           => 1,
            'qps_service_uri' => 'udp://10.17.46.36:3000',
            'failed_qps_service_uri' => 'udp://10.17.46.36:3010',
            'log_service_uri' => 'udp://10.17.46.36:3020',
        ),

        // 查询接口调用结果 只开一个进程 已经配置好，不用再配置
        'StatisticGlobal' => array(
            'protocol'              => 'tcp',
            'port'                  => 20203,
            'child_count'           => 1,
        ),

        // 查询接口调用结果 只开一个进程 已经配置好，不用再配置
        'StatisticProvider' => array(
            'protocol'              => 'tcp',
            'port'                  => 20204,
            'child_count'           => 4,
        ),

        // 监控server框架的worker 只开一个进程 framework里面需要配置成线上参数
        'Monitor' => array(
            'protocol'              => 'tcp',
            'port'                  => 20305,
            'child_count'           => 1,
            'framework'             => array(
                 'phone'   => '18682754515,18980878157,13541296429,18380308158',      // 告警电话
                 'url'     => 'http://sms.int.jumei.com/send',  // 发送短信调用的url
                 'param'   => array(                            // 发送短信用到的参数
                     'channel' => 'monternet',
                     'key'     => 'notice_rt902pnkl10udnq',
                     'task'    => 'int_notice',
                 ),
                 'min_success_rate' => 98,                    // 框架层面成功率小于这个值时触发告警
                 'max_worker_normal_exit_count' => 1000,      // worker进程退出（退出码为0）次数大于这个值时触发告警
                 'max_worker_unexpect_exit_count' => 10,      // worker进程异常退出（退出码不为0）次数大于这个值时触发告警
             )
        ),

        'ThriftEncrypt' => array(
            'protocol'              => 'tcp',
            'port'                  => 3202,
            'child_count'           => 21,
            'persistent_connection' => false,
            'provider'              => '/home/www/TrusteeshipData/Provider',
            'handler'               => '/home/www/TrusteeshipData/Handler',
            'bootstrap'             => '/home/www/TrusteeshipData/init.php',
            'max_requests'          => 1000,
            'worker_class'          => 'ThriftWorker',
        ),

        // [开发环境用，生产环境可以去掉该项]rpc web测试工具
        'TestClientWorker' => array(
            'protocol'              => 'tcp',
            'port'                  => 30303,
            'child_count'           => 1,
        ),

        // [开发环境用，生产环境可以去掉该项]thrift rpc web测试工具
        'TestThriftClientWorker' => array(
            'protocol'              => 'tcp',
            'port'                  => 30304,
            'child_count'           => 1,
        ),

        'ServiceRegister' => array(
            'protocol'              => 'tcp',
            'port'                  => 23333,
            'child_count'           => 1,
            'worker_class'          => 'ServiceRegister',
        ),
        'QuotaAgent' => array(
            'protocol'              => 'udp',
            'port'                  => 1984,
            'child_count'           => 1,
            'send_timeout'          => 10,
            'persistent_connection' => true,
            'quota_service_uri'     => 'tcp://10.17.46.36:2000',
        ),
    ),

    'backlog'      => 65535,
    'ENV'          => 'production', // dev or production
    'worker_user'  => 'www-data', //运行worker的用户,正式环境应该用低权限用户运行worker进程

    'project_name' => 'TrusteeshipData',  //必须与配置系统中项目名称一致

    // 数据签名用私匙
    'rpc_secret_key'    => '769af463a39f077a0340a189e9c1ec28',

    // 日志追踪 trace_log 日志目录
    'trace_log_path'    => '/home/logs/monitor',
    // 异常监控 exception_log 日志目录
    'exception_log_path'=> '/home/logs/monitor',
    // 是否开启日志追踪监控
    'trace_log_on'      => true,
    // 是否开启异常监控
    'exception_log_on'  => true,
    // 日志追踪采样，10代表 采样率1/10, 100代表采样率1/100
    'trace_log_sample'  => 10,
);