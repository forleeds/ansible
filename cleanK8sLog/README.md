
### 清理k8s node节点业务日志
 
**1.功能描述**   

k8s node节点上面的pods在编排的时候将会挂载一个目录到node的/var/lib/container/logs,用于临时存放业务日志，由于业务日志比较大，需要每隔一段时间（1-4h）进行清理，此脚本就是为了实现向node节点推送清理脚本并设置定时清理任务。  


**2.执行方式**  
clone之后，进入cleanK8sLog目录，将node的IP添加到hosts文件再执行以下命令即可。     

```
#测试
ansible -i hosts node -m ping

#执行
ansible-playbook clean.yaml -e server=node -i hosts   
```
