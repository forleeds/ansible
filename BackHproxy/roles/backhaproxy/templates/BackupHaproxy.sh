#!/bin/bash
#定时备份haproxy配置数据
#2018.09
set -e
tdate=`date +%Y%m%d%H`
bdir=/home/jm/backuphaproxy

[ -d $bdir/$tdate ] && echo "Dir is exsit" || mkdir -p $bdir/$tdate
/bin/cp -pr /etc/haproxy/haproxy.tmpl /etc/haproxy/*.sites /etc/haproxy/white_list* $bdir/$tdate/