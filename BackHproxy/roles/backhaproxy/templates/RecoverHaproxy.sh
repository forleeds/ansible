#!/bin/bash
#恢复haproxy配置数据
#2018.09
set -e
bdir=/home/jm/backuphaproxy

if [ "$1" ];then
    if [ -f $bdir/$1/haproxy.tmpl ];then
        /etc/init.d/consul-template stop &&  /bin/cp -pr $bdir/$1/haproxy.tmpl $bdir/$1/*.sites /etc/haproxy/
        echo ""
        /etc/init.d/haproxy reload
    else
        echo "Please check  parameters" && exit 1
    fi
else
    echo "Please specify parameters" && exit 2
fi