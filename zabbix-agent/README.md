
### 清理k8s node节点业务日志
 
**1.功能描述**   

k8s node节点上面安装zabbix-agent使用  


**2.执行方式**  
clone之后，进入zabbix-agent目录，将node的IP添加到hosts文件再执行以下命令即可。     

```
#测试
ansible -i hosts node -m ping

#执行
ansible-playbook agent.yaml -e server=node -i hosts   
```
