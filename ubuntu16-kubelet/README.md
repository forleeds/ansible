### 简介

此Ansible主要用于初始化聚美物理机接入到Kubernetes的标准安装脚本，其中包含了如下内容：

- 重置ROOT密码，主机Hostname，SSH服务，时区等
- 调整内核参数，优化系统配置
- 安装系统基础安装包
- 更新OPS资产信息
- 调整服务器网卡
- 安装ldap客户端，zabbix客户端、CNI组建、docker/kubelet客户端


### 用法

#### 初始化系统

```
ansible-playbook base.yaml -e s=new_docker -i hosts -f 10
```

#### 安装zabbix

```
ansible-playbook zabbix.yaml -e s=new_docker -i hosts -f 10
```

#### 安装ldap客户端

```
ansible-playbook access.yaml -e s=new_docker -i hosts -f 10
```

#### 安装kubelet客户端

```
ansible-playbook kubelet.yaml -e s=new_docker -i hosts -f 10
```

#### 安装cni组建

```
ansible-playbook cni.yaml -e s=<宿主机IP地址> -i ipam_hosts.py
```
