#!/usr/bin/python
# -*- coding: utf-8 -*-
import json
import urllib2
import sys
import requests
from sys import exit

global_url_ext = "https://ops.int.jumei.com"
global_url_int= "http://api.ops.int.jumei.com"

def ops_get_host(local_ip):
    result = requests.get(global_url_int+"/api/asset/host/get-by-ipaddr/?ip_addr=%s" % (local_ip))
    if int(result.status_code) != 200:
        result = requests.get(global_url_ext+"/api/asset/host/get-by-ipaddr/?ip_addr=%s" % (local_ip))
        if int(result.status_code) !="200":
            exit(0)

    rsp = json.loads(result.content)

    if int(rsp['code']) == 0:
        # product 1, loadbench 4
        if int(rsp['data']['environment']) == 1:
            environment = "product"
        elif int(rsp['data']['environment']) == 4:
            environment = "loadbench"
        else:
            environment = "test"

        # used 0 or unused 1
        if rsp['data']['unused'] == False:
            status = "used"
        else:
            status = "unused"

        project = rsp['data']['project_unique_name']
    else:
        environment = ""
        status = "unused"
        project = ""

    cmdb_info = "^" + project + "#" + environment + "@" + status + "$"
    return cmdb_info

def get_local_ip():
    local_ip = requests.get('http://10.0.18.10/ip').content
    return local_ip.strip()

def main():
    local_ip = get_local_ip()
    cmdb_info = ops_get_host(local_ip)
    print cmdb_info

if __name__ == "__main__":
    main()

