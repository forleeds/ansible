#!/bin/bash

kubectl config set-cluster squirrel-cluster --server=https://192.168.17.102:8443 --certificate-authority=/home/huxos/Code/ubuntu_ansible/roles/k8s-master/files/etc/kubernetes/ssl/ca.pem
kubectl config set-credentials squirrel-admin --certificate-authority=/home/huxos/Code/ubuntu_ansible/roles/k8s-master/files/etc/kubernetes/ssl/ca.pem --client-key=/home/huxos/Code/ubuntu_ansible/roles/k8s-master/files/etc/kubernetes/ssl/squirrel-key.pem --client-certificate=/home/huxos/Code/ubuntu_ansible/roles/k8s-master/files/etc/kubernetes/ssl/squirrel.pem
kubectl config set-context squirrel-system --cluster=squirrel-cluster --user=squirrel-admin
kubectl config use-context squirrel-system
