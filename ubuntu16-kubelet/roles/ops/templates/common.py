#!/usr/bin/env python
# encoding: utf-8

import urllib
import urllib2
import commands

def switch_info():
    results = commands.getoutput(
        "timeout 30 tcpdump -v -s 1500 -c 1 '(ether[12:2]=0x88cc or ether[20:2]=0x2000)' 2>/dev/null")
    info = {}
    for line in results.split('\n'):
        if ("Device-ID " in line or "System Name TLV" in line):
            switcher_name = line.split(' ')[-1].replace("'", "")
            info['access_switch_physical_id'] = switcher_name
            info['idc_room'] = switcher_name.split('-')[0]
            info['idc_cabinet'] = switcher_name.split(
                '-')[1] if len(switcher_name.split('-')) > 1 else ''
            continue
        if ("Port-ID " in line or "Subtype Interface Name" in line):
            info['access_switch_physical_port'] = line.split(
                ' ')[-1].replace("'", "")
            continue
        if ("Native VLAN ID" in line or "port vlan id" in line):
            info['access_switch_vlan'] = line.split(
                ' ')[-1].replace("'", "")
            continue
    return info

if __name__ == '__main__':
    url = 'http://api.ops.int.jumei.com/plugin/host/update-fields/?api_key=dd0620c3bbae97d2e450c54140a242bc'
    info = {
        'ip_addr': '{{inventory_hostname}}',
        'manufacturer': '{{ansible_system_vendor}}',
        'productname': '{{ansible_product_name}}',
        'cpu_model': '{{ansible_processor[1]}}',
        'kernel_release': '{{ansible_kernel}}',
        'cpu_threads': '{{ansible_processor_vcpus}}',
        'mem_total': '{{ansible_memtotal_mb}}',
        'os_family': '{{ansible_os_family}}',
        'os_release': '{{ansible_distribution}}-{{ansible_distribution_version}}',
        'product_sn': '{{ansible_product_serial}}',
        'mac_addr': '{{ansible_eth0.macaddress}}'.upper(),
        'product_uuid': '{{ansible_product_uuid}}',
        'type': 1,
    }
    info.update(switch_info())
    data = urllib.urlencode(info)
    req = urllib2.Request(url, data)
    urllib2.urlopen(req)
