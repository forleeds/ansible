#!/bin/bash
#openssl genrsa -out ca-key.pem 2048
#openssl req -x509 -new -nodes -key ca-key.pem -days 10000 -out ca.pem -subj "/CN=kube-ca"

#openssl genrsa -out apiserver-key.pem 2048
#openssl req -new -key apiserver-key.pem -out apiserver.csr -subj "/CN=kube-apiserver" -config openssl.cnf
#openssl x509 -req -in apiserver.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out apiserver.pem -days 3650 -extensions v3_req -extfile openssl.cnf
#
#openssl genrsa -out worker-key.pem 2048
#openssl req -new -key worker-key.pem -out worker.csr -subj "/O=system:nodes/CN=kube-worker"
#openssl x509 -req -in worker.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out worker.pem -days 3650
#
#openssl genrsa -out admin-key.pem 2048
#openssl req -new -key admin-key.pem -out admin.csr -subj "/CN=kube-admin"
#openssl x509 -req -in admin.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out admin.pem -days 3650
#
#openssl genrsa -out proxy-key.pem 2048
#openssl req -new -key proxy-key.pem -out proxy.csr -subj "/CN=system:kube-proxy"
#openssl x509 -req -in proxy.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out proxy.pem -days 3650
#
#openssl genrsa -out master-key.pem 2048
#openssl req -new -key master-key.pem -out master.csr -subj "/O=system:masters/CN=kube-master"
#openssl x509 -req -in master.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out master.pem -days 3650

#openssl genrsa -out squirrel-key.pem 2048
#openssl req -new -key squirrel-key.pem -out squirrel.csr -subj "/CN=squirrel"
#openssl x509 -req -in squirrel.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out squirrel.pem -days 3650

#openssl genrsa -out squirrel-key.pem 2048
#openssl req -new -key squirrel-key.pem -out squirrel.csr -subj "/CN=squirrel"
#openssl x509 -req -in squirrel.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out squirrel.pem -days 3650

openssl genrsa -out fabric8-key.pem 2048
openssl req -new -key fabric8-key.pem -out fabric8.csr -subj "/CN=fabric8"
openssl x509 -req -in fabric8.csr -CA ca.pem -CAkey ca-key.pem -CAcreateserial -out fabric8.pem -days 3650
