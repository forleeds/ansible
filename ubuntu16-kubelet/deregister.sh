#!/bin/bash

PROJECT=$(curl -s http://127.0.0.1:8500/v1/agent/services?pretty |grep ID  |awk -F'"' '{print $4}')

curl -X PUT  "http://127.0.0.1:8500/v1/agent/service/deregister/${PROJECT}"
